import axios from "axios";

const api = axios.create({
  baseURL: "https://kenziedoc.herokuapp.com/",
});

export default api;
