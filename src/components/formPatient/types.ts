export interface PatientFormProps {
  age: number;
  cpf: string;
  email: string;
  gender: string;
  health_insurance: string;
  name: string;
  phone: string;
  permission: string;
  password: string;
}
