import { Link, useNavigate } from "react-router-dom";
import { ContainerHeader } from "./style";

export const Header = () => {
  const navigate = useNavigate();
  return (
    <>
      <ContainerHeader>
        <nav>
          <ul>
            <li>
              <button onClick={() => navigate("/")}>Home</button>
            </li>
            <li>
              <button onClick={() => navigate("/dashboard")}>Dashboard</button>
            </li>
            <li>
              <button onClick={() => navigate("/login")}>Login</button>
            </li>
          </ul>
        </nav>
      </ContainerHeader>
    </>
  );
};
