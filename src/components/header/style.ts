import styled from "styled-components";

export const ContainerHeader = styled.header`
  background-color: red;
  display: flex;
  justify-content: center;
  align-items: center;

  ul {
    width: 100%;
    height: 10vh;
    display: flex;
    justify-content: space-around;
    list-style: none;
  }
  li {
    padding: 30px;
  }
`;
