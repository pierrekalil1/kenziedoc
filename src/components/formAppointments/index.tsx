import { Input } from "../input";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schema } from "./validators";
import { AppointmentsFormProps } from "./types";
import { Button, ContainerForm } from "./styles";
import { useAppointment } from "../../provider/Appointments";
import appointmentsImg from "../../assets/appointmentsImg.svg";
import { useState } from "react";

export const FormAppointments = () => {
  const { createAppointments } = useAppointment();
  const [date1, setDate] = useState("".replace("-", "/"));

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmitData = (data: AppointmentsFormProps) => {
    const { date, professional, patient, time } = data;
    const newdate = date1 + "T" + time + ":00" + "Z";
    const newdata = {
      professional: professional,
      patient: patient,
      newdate: newdate,
    };
    createAppointments(newdata);

    console.log(professional, patient, newdate);
    // reset();
  };

  return (
    <ContainerForm>
      <form onSubmit={handleSubmit(onSubmitData)}>
        <div className="test">
          <span>Marcar consulta</span>
          <img src={appointmentsImg} alt="" />
        </div>
        <div className="inputs">
          <Input
            colorInput
            type="text"
            placeholder="CPF do paciente"
            register={register}
            name="patient"
            error={errors.patient?.message}
          />
          <Input
            colorInput
            type="text"
            placeholder="CRM do médico"
            register={register}
            name="professional"
            error={errors.professional?.message}
          />
          <Input
            colorInput
            type="date"
            placeholder="Data da consulta"
            register={register}
            name="date"
            error={errors.date?.message}
            onChange={(e) => setDate(e.target.value)}
          />
          <Input
            colorInput
            type="time"
            placeholder="Hora da consulta"
            register={register}
            name="time"
            error={errors.time?.message}
            step="2"
            // onChange={(e) => setDate(e.target.value)}
          />
          <Button type="submit">Login</Button>
        </div>
      </form>
    </ContainerForm>
  );
};
