import styled from "styled-components";

export const ContainerHome = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  height: 100vh;
  width: 100%;

  background-image: linear-gradient(
    to bottom right,
    rgb(68, 196, 249, 100%),
    rgb(31, 96, 203, 100%)
  );
`;
