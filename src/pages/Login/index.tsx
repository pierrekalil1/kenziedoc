import { FormLogin } from "../../components/formLogin";
import { ContanerLogin } from "./style";

export const Login = () => {
  return (
    <>
      <ContanerLogin>
        <h1>Login</h1>
        <FormLogin />
      </ContanerLogin>
    </>
  );
};
