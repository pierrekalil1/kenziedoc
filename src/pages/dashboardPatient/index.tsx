import { FormAppointments } from "../../components/formAppointments";
import { useAuth } from "../../provider/Auth";
import { ContainerMain } from "./style";

export const DashboardPatient = () => {
  const { user, identifyUser } = useAuth();
  return (
    <ContainerMain>
      <h1>Paciente {identifyUser}</h1>
      <FormAppointments />
    </ContainerMain>
  );
};
