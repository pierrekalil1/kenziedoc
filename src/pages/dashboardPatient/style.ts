import styled from "styled-components";

export const ContainerMain = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
export const HeaderPatient = styled.div`
  display: flex;
  justify-content: space-around;
  height: 10vh;
  width: 100%;
  background-color: var(--blueBackground);
`;
