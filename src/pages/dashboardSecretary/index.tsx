import { useAuth } from "../../provider/Auth";
import { usePatient } from "../../provider/Patient";

export const DashboardSecretary = () => {
  const { identifyUser, user } = useAuth();
  const { allPatient } = usePatient();
  console.log(identifyUser);
  return (
    <>
      <h1>Admin {identifyUser}</h1>
    </>
  );
};
