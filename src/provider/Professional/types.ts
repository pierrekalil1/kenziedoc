import { ReactNode } from "react";

export interface ProfessionalProviderProps {
  children: ReactNode;
}

export interface ProfessionalDataProps {
  FilterByProfessional: (identify: string) => void;
}
