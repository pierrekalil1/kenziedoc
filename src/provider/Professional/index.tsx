import { createContext, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import api from "../../services/api";
import { useAuth } from "../Auth";
import { ProfessionalDataProps, ProfessionalProviderProps } from "./types";

const ProfessionalContext = createContext<ProfessionalDataProps>(
  {} as ProfessionalDataProps
);

export const ProfessionalProvider = ({
  children,
}: ProfessionalProviderProps) => {
  const { token, setUser, identifyUser } = useAuth();
  const [allProfessional, setAllProfessional] = useState<[]>([]);
  const navigate = useNavigate();

  const FilterByProfessional = async (identify: string) => {
    await api
      .get(`/professional/${identify}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        setUser(res.data);
        // navigate("/dashboardprofessional");
      })
      .catch((e) => console.log(e, "erro get by email"));
  };

  useEffect(() => {
    if (token && identifyUser == "Admin") {
      api
        .get("/professional", {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((res) => {
          console.log(res.data);
          setAllProfessional(res.data);
        })
        .catch((e) => console.log(e, "erro professional"));
    }
    FilterByProfessional;
  }, []);

  return (
    <ProfessionalContext.Provider value={{ FilterByProfessional }}>
      {children}
    </ProfessionalContext.Provider>
  );
};

export const useProfessional = () => useContext(ProfessionalContext);
