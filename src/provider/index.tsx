import { AppointmentsProvider } from "./Appointments";
import { AuthProvider } from "./Auth";
import { AuthProviderProps } from "./Auth/types";
import { PatientProvider } from "./Patient";
import { ProfessionalProvider } from "./Professional";

export const Providers = ({ children }: AuthProviderProps) => {
  return (
    <>
      <AuthProvider>
        <ProfessionalProvider>
          <PatientProvider>{children}</PatientProvider>
        </ProfessionalProvider>
      </AuthProvider>
    </>
  );
};
